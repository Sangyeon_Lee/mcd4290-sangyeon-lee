function doIt(){
    let number1Ref=document.getElementById("number1");
    let number2Ref=document.getElementById("number2");
    let number3Ref=document.getElementById("number3");
    let answerRef=document.getElementById("answer");
    let parityRef=document.getElementById("parity");
    let number1=Number(number1Ref.value);
    let number2=Number(number2Ref.value);
    let number3=Number(number3Ref.value);
    let answer=number1+number2+number3;
   
  
    if(answer>0)
        {
            answerRef.innerHTML=answer;
            answerRef.className="positive"
        }
    else
        {
            answerRef.innerHTML=answer;
            answerRef.className="negative"
        }
    if(answer%2===0)
        {
            parity.innerHTML="(Even)"
            parity.className="even"
        }
    else
        {
            parity.innerHTML="(Odd)"
            parity.className="odd"
        }
}